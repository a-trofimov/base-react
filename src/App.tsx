import React, { useState } from 'react';
import './App.css';

function App() {

  const [uname, setUname] = useState('');
  const [pass, setpass] = useState('');

  const handleSubmit = (e: any) => {

    // Prevent page reload
    e.preventDefault();
    
    fetch('/api/login', {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({login: uname, password: pass}),
    });
  };

  const renderForm = (
    <div className="form">
        <form onSubmit={handleSubmit}>
          <div className="input-container">
            <label>Телефон или почта </label>
            <input type="text" name="uname" onChange={e => setUname(e.target.value)} value={uname} required />
          </div>
          <div className="input-container">
            <label>Пароль </label>
            <input type="password" name="pass" onChange={e => setpass(e.target.value)} value={pass} required />
          </div>
          <div className="button-container">
            <input type="submit" />
          </div>
        </form>
      </div>    
  );

  return (
    <div className="app">
      <div className="login-form">
        <div className="title">Вход</div>
        {renderForm}
      </div>
    </div>
  );
}

export default App;
